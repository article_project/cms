Rake::Task['db:seed:setup:main'].invoke

Rake::Task['db:seed:development:parsing_tasks'].invoke
Rake::Task['db:seed:development:parsing_pages'].invoke
Rake::Task['db:seed:development:parsing_articles'].invoke

Rake::Task['db:seed:development:rewrite_task_templates'].invoke
Rake::Task['db:seed:development:rewrite_tasks'].invoke
Rake::Task['db:seed:development:rewrite_task_results'].invoke

Rake::Task['db:seed:development:articles'].invoke
