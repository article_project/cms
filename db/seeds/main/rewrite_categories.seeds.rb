DatabaseCleaner.clean_with(:truncation, only: %i[rewrite_categories])
seed_file = Rails.root.join('db', 'seeds', 'main', 'rewrite_categories.seeds.yml')
data = YAML.load_file(seed_file)

p('rewrite_categories...')
Rewrite::Category.create!(data)
