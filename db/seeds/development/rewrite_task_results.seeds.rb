DatabaseCleaner.clean_with(:truncation, only: %i[rewrite_task_results])
seed_file = Rails.root.join('db', 'seeds', 'development', 'rewrite_task_results.seeds.yml')
data = YAML.load_file(seed_file)

p('rewrite_task_results...')
Rewrite::Task::Result.transaction do
  data.each do |item|
    Rewrite::Task::Result.create!(item.merge(made_at: DateTime.now - 1.day))
  end
end
