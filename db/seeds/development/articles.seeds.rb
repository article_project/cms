DatabaseCleaner.clean_with(:truncation, only: %i[articles])
seed_file = Rails.root.join('db', 'seeds', 'development', 'articles.seeds.yml')
data = YAML.load_file(seed_file)

p('articles...')
Article.transaction do
  data.each do |article|
    article['body'] = File.read(Rails.root.join('db', 'seeds', 'development', 'articles', article['body']))
    Article.create!(article)
  end
end
