DatabaseCleaner.clean_with(:truncation, only: %i[parsing_tasks])
seed_file = Rails.root.join('db', 'seeds', 'development', 'parsing_tasks.seeds.yml')
data = YAML.load_file(seed_file)

p('parsing_tasks...')
Parsing::Task.create!(data)
