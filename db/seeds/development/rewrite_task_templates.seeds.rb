DatabaseCleaner.clean_with(:truncation, only: %i[rewrite_task_templates])
seed_file = Rails.root.join('db', 'seeds', 'development', 'rewrite_task_templates.seeds.yml')
data = YAML.load_file(seed_file)

p('rewrite_task_templates...')
Rewrite::Task::Template.create!(data)
