DatabaseCleaner.clean_with(:truncation, only: %i[rewrite_tasks])
seed_file = Rails.root.join('db', 'seeds', 'development', 'rewrite_tasks.seeds.yml')
data = YAML.load_file(seed_file)

p('rewrite_tasks...')
Rewrite::Task.transaction do
  data.each do |item|
    if item['status'].zero?
      Rewrite::Task.create!(item)
    else
      Rewrite::Task.create!(item.merge(deadline: DateTime.now + item['deadline_days'].to_i.days))
    end
  end
end
