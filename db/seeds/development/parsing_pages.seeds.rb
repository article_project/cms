DatabaseCleaner.clean_with(:truncation, only: %i[parsing_pages])
seed_file = Rails.root.join('db', 'seeds', 'development', 'parsing_pages.seeds.yml')
data = YAML.load_file(seed_file)

p('parsing_pages...')
Parsing::Page.create!(data)
