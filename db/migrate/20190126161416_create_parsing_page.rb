class CreateParsingPage < ActiveRecord::Migration[5.2]
  def change
    create_table :parsing_pages do |t|
      t.bigint :task_id, null: false

      t.string :address, null: false, limit: 512, unique: true
      t.boolean :hrefs_has_been_parsed, null: false, default: false
      t.boolean :articles_has_been_parsed, null: false, default: false

      t.timestamps

      t.index :task_id
    end
  end
end
