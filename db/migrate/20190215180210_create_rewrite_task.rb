class CreateRewriteTask < ActiveRecord::Migration[5.2]
  def change
    create_table :rewrite_tasks do |t|
      t.bigint :parsing_article_id, null: false
      t.bigint :template_id

      t.string :title, null: false
      t.text :description
      t.integer :price_1ks, null: false
      t.integer :status, null: false, default: 0
      t.integer :deadline_days, null: false
      t.date :deadline
      t.integer :category_id, null: false

      t.string :external_id

      t.timestamps

      t.index :parsing_article_id, unique: true
      t.index :category_id
    end
  end
end
