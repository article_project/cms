class AddBlockRequestToParsingTask < ActiveRecord::Migration[5.2]
  def change
    add_column :parsing_tasks, :block_request_path, :string
    add_column :parsing_tasks, :block_request_value, :string
  end
end
