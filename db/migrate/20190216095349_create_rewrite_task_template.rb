class CreateRewriteTaskTemplate < ActiveRecord::Migration[5.2]
  def change
    create_table :rewrite_task_templates do |t|
      t.string :title, null: false

      t.string :task_title, null: false
      t.text :task_description, default: ''
      t.integer :price_1ks, null: false
      t.integer :deadline_days, null: false
      t.integer :category_id, null: false

      t.timestamps

      t.index :category_id
    end
  end
end
