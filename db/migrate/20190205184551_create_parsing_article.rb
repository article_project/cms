class CreateParsingArticle < ActiveRecord::Migration[5.2]
  def change
    create_table :parsing_articles do |t|
      t.bigint :page_id, null: false

      t.text :body, unique: true
      t.integer :status, null: false, default: 0
      t.string :title, null: false

      t.timestamps

      t.index :page_id
      t.index %i[page_id title], unique: true
    end
  end
end
