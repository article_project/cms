class CreateProxy < ActiveRecord::Migration[5.2]
  def change
    create_table :proxies do |t|
      t.string :ip, null: false
      t.integer :port, null: false

      t.index %i[ip port], unique: true
    end
  end
end
