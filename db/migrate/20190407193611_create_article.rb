class CreateArticle < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.integer :rewrite_task_result_id
      t.string :title, null: false, unique: true
      t.string :link, null: false, unique: true
      t.string :description, limit: 512, null: false
      t.text :body, null: false, unique: true
      t.integer :status, null: false, default: 0
      t.datetime :publish_at, null: false
      t.timestamps
      t.index :title, unique: true
      t.index :link, unique: true
    end
  end
end
