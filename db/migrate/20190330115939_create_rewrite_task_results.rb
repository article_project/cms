class CreateRewriteTaskResults < ActiveRecord::Migration[5.2]
  def change
    create_table :rewrite_task_results do |t|
      t.integer :task_id, null: false
      t.string :external_id, null: false
      t.string :performer_comment, null: false, default: ''
      t.string :my_comment
      t.datetime :made_at, null: false
      t.string :per_mistakes
      t.string :per_antiplagiat
      t.string :per_diff
      t.string :result_url, null: false, limit: 512
      t.integer :status, null: false, default: 0
      t.boolean :system_checked, null: false

      t.timestamps

      t.index :task_id
      t.index :external_id, unique: true
    end
  end
end
