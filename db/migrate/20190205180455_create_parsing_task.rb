class CreateParsingTask < ActiveRecord::Migration[5.2]
  def change
    create_table :parsing_tasks do |t|
      t.string :host, unique: true
      t.integer :status, null: false, default: 0
      t.string :article_body_path, null: false
      t.string :article_title_path, null: false

      t.timestamps
    end
  end
end
