class AddIndexOnlyOneArticleOfRewriteResult < ActiveRecord::Migration[5.2]
  def change
    add_index :articles, :rewrite_task_result_id, unique: true
  end
end
