class AddResutToRewriteTaskResults < ActiveRecord::Migration[5.2]
  def change
    add_column :rewrite_task_results, :result, :string, null: false, default: ''
  end
end
