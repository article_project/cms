# article-cms

## Project setup
```
gem install bundler
bundle update
```

### Compiles and hot-reloads for development
```
rails s
```

### Production build
```
docker-compose build
docker-compose up -d
docker-compose exec web /bin/sh
  export DATABASE_CLEANER_ALLOW_PRODUCTION=true
  export DATABASE_CLEANER_ALLOW_REMOTE_DATABASE_URL=true
  bundle exec rake db:seed
  export DATABASE_CLEANER_ALLOW_PRODUCTION=false
  export DATABASE_CLEANER_ALLOW_REMOTE_DATABASE_URL=false
```

### Push image to docker hub
```
docker-compose push web
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
