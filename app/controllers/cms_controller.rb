class CmsController < ApplicationController
  layout 'cms'

  http_basic_authenticate_with(
    name: Rails.application.credentials.cms[:login] || 'login',
    password: Rails.application.credentials.cms[:pass] || 'nF*Jc24lesj'
  )
end
