class Api::ArticlesController < ApiController
  def index
    @articles = Article.open
    render(json: @articles.map(&:json_for_api))
  end

  def show
    @article = Article.find_by!(link: params[:id])
    if @article.open?
      render(json: @article.json_for_api)
    else
      render(json: {}, status: 406)
    end
  rescue ActiveRecord::RecordNotFound
    render(json: {}, status: 404)
  end
end
