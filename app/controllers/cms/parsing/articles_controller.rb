class Cms::Parsing::ArticlesController < CmsController
  include BootstrapTable

  before_action :table_headers, only: %i[index all]
  before_action :form_fields, only: %i[edit update]

  def index
    @page = ::Parsing::Page.find_by!(id: params[:page_id], task_id: params[:task_id])
    args = pagination_params
    @articles = @page.articles.page(args[:page]).per(args[:per])
    @total = @page.articles.count
    basic_breadcrumbs(@page)
  rescue ActiveRecord::RecordNotFound
    redirect_to(:cms_parsing_tasks_path)
  end

  def all
    args = pagination_params
    @articles = ::Parsing::Article.active.page(args[:page]).per(args[:per])
    @total = ::Parsing::Article.count
    add_breadcrumb('Articles', :cms_parsing_all_articles_path)
  end

  def edit
    @article = ::Parsing::Article.find(params[:id])
    basic_breadcrumbs(@article.page)
    add_breadcrumb(@article.id, edit_cms_parsing_task_page_article_path(@article))
  rescue ActiveRecord::RecordNotFound
    redirect_to(:cms_parsing_tasks_path)
  end

  def update
    @article = ::Parsing::Article.find(params[:id])
    basic_breadcrumbs(@article.page)
    add_breadcrumb(@article.id, edit_cms_parsing_task_page_article_path(@article))
    @article.update!(article_params)
    redirect_to(action: :index, page_id: @article.page_id, task_id: @article.page.task_id)
  rescue ActiveRecord::RecordNotFound
    redirect_to(action: :index, page_id: @article.page_id, task_id: @article.page.task_id)
  rescue ActiveRecord::RecordInvalid => e
    @errors = e.record.errors.messages
    render(action: :edit)
  end

  private

    # Хедеры для таблицы на странице index
    def table_headers
      @table_headers = [{
        field: 'id',
        title: '#',
      }, {
        field: 'address',
        title: 'Address',
      }, {
        field: 'status',
        title: 'Status',
      }, {
        field: 'edit',
        title: 'Edit',
      },]
    end

    # Поля для формы8080
    def form_fields
      @form_fields = [{
        type: FormFieldType::RADIO_BUTTON,
        key: :status,
        buttons: [{
          value: ::Parsing::Article::NEW,
          label: 'New',
        }, {
          value: ::Parsing::Article::NOT_ARTICLE,
          label: 'Not article',
        }, {
          value: ::Parsing::Article::FOR_REWRITE,
          label: 'For rewrite',
        }, {
          value: ::Parsing::Article::SENT_TO_REWRITE,
          label: 'Sent to rewrite',
        }, {
          value: ::Parsing::Article::REWRITED,
          label: 'Rewrited',
        },],
      }, {
        type: FormFieldType::TEXT_FIELD,
        key: :title,
        label: 'Title'
      }, {
        type: FormFieldType::TEXT_AREA,
        key: :body,
        label: 'Body',
      },]
    end

    def basic_breadcrumbs(page)
      add_breadcrumb('Parsing tasks', :cms_parsing_tasks_path)
      add_breadcrumb(page.task.host, edit_cms_parsing_task_path(page.task_id))
      add_breadcrumb('Pages', cms_parsing_task_pages_path(task_id: page.task_id))
      add_breadcrumb('Articles', cms_parsing_task_page_articles_path(task_id: page.task_id, page_id: page.id))
    end

    def article_params
      params.require(:parsing_article).permit(:html, :status, :title, :body)
    end
end
