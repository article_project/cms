class Cms::Parsing::TasksController < CmsController
  include BootstrapTable

  add_breadcrumb('Parsing tasks', :cms_parsing_tasks_path)
  before_action :table_headers, only: %i[index]
  before_action :form_fields, only: %i[new edit update create]

  def index
    args = pagination_params
    @tasks = ::Parsing::Task.page(args[:page]).per(args[:per])
    @total = ::Parsing::Task.count

    respond_to do |format|
      format.html
      format.json
    end
  end

  def new
    add_breadcrumb('New task', :new_cms_parsing_task_path)
    @task = ::Parsing::Task.new
  end

  def create
    add_breadcrumb('New task', :new_cms_parsing_task_path)

    # Создание в 2 шага что бы передать форме валидные данные а не nil (в случае ошибки валидации)
    @task = ::Parsing::Task.new(task_params)
    @task.save!

    redirect_to(action: :index)
  rescue ActiveRecord::RecordInvalid => e
    @errors = e.record.errors.messages
    render(action: :new, status: 422)
  end

  def edit
    @task = ::Parsing::Task.find(params[:id])
    add_breadcrumb(@task.host, edit_cms_parsing_task_path(@task))
  rescue ActiveRecord::RecordNotFound
    redirect_to(:cms_parsing_tasks_path)
  end

  def update
    @task = ::Parsing::Task.find(params[:id])
    @task.update!(task_params)
    redirect_to(action: :index)
  rescue ActiveRecord::RecordNotFound
    render(nothing: true, status: 404)
  rescue ActiveRecord::RecordInvalid => e
    add_breadcrumb(e.record.host, edit_cms_parsing_task_path(@task))
    @errors = e.record.errors.messages
    render(action: :edit, status: 422)
  end

  private

    # Хедеры для таблицы на странице index
    def table_headers
      @table_headers = [{
        field: 'id',
        title: '#',
      }, {
        field: 'host',
        title: 'Host',
      }, {
        field: 'status',
        title: 'Status',
      }, {
        field: 'edit',
        title: 'Edit',
      }, {
        field: 'pages',
        title: 'Pages',
      },]
    end

    # Поля для формы
    def form_fields
      @form_fields = [{
        type: FormFieldType::TEXT_FIELD,
        key: :host,
        label: 'Host',
      }, {
        type: FormFieldType::TEXT_FIELD,
        key: :article_title_path,
        label: 'Article title path',
      }, {
        type: FormFieldType::TEXT_FIELD,
        key: :article_body_path,
        label: 'Article body path',
      }, {
        type: FormFieldType::TEXT_FIELD,
        key: :block_request_path,
        label: 'Article bad request path',
      }, {
        type: FormFieldType::TEXT_FIELD,
        key: :block_request_value,
        label: 'Article bad request value',
      }, {
        type: FormFieldType::RADIO_BUTTON,
        key: :status,
        buttons: [{
          value: ::Parsing::Task::INACTIVE,
          label: 'Inactive',
        }, {
          value: ::Parsing::Task::ACTIVE,
          label: 'Active',
        }, {
          value: ::Parsing::Task::ARCHIVED,
          label: 'Archived',
        },],
      },]
    end

    # Аргументы для создания и обновления таска
    def task_params
      params.require(:parsing_task)
            .permit(:host, :article_title_path, :article_body_path, :status, :block_request_path, :block_request_value)
    end
end
