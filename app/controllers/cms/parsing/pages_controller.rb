class Cms::Parsing::PagesController < CmsController
  include BootstrapTable

  before_action :table_headers, only: %i[index]

  layout :resolve_layout

  def index
    @task = ::Parsing::Task.find(params[:task_id])
    args = pagination_params
    @pages = @task.pages.page(args[:page]).per(args[:per])
    @total = @task.pages.count
    basic_breadcrumbs(@task)
  rescue ActiveRecord::RecordNotFound
    redirect_to(:cms_parsing_tasks_path)
  end

  def show
    @page = ::Parsing::Page.find_by!(id: params[:id], task_id: params[:task_id])
    basic_breadcrumbs(@page.task)
    add_breadcrumb(@page.address, cms_parsing_task_page_path(id: @page.id, task_id: @page.task_id))
  rescue ActiveRecord::RecordNotFound
    redirect_to(:cms_parsing_tasks_path)
  end

  def body
    page = ::Parsing::Page.find_by!(id: params[:id], task_id: params[:task_id])
    @body = page.body
  rescue ActiveRecord::RecordNotFound
    redirect_to(:cms_parsing_tasks_path)
  end

  private

    def resolve_layout
      'blank' if action_name == 'body'
    end

    # Хедеры для таблицы на странице index
    def table_headers
      @table_headers = [{
        field: 'id',
        title: '#',
      }, {
        field: 'address',
        title: 'Address',
      }, {
        field: 'hrefs_has_been_parsed',
        title: 'Hrefs parsed',
      }, {
        field: 'articles_has_been_parsed',
        title: 'Articles parsed',
      }, {
        field: 'articles',
        title: 'Articles',
      }, {
        field: 'body',
        title: 'Содержание',
      },]
    end

    def basic_breadcrumbs(task)
      add_breadcrumb('Parsing tasks', :cms_parsing_tasks_path)
      add_breadcrumb(task.host, edit_cms_parsing_task_path(task))
      add_breadcrumb('Pages', cms_parsing_task_pages_path(task_id: task.id))
    end
end
