class Cms::ArticlesController < CmsController
  include BootstrapTable

  add_breadcrumb('Articles', :cms_articles_path)
  before_action :table_headers, only: %i[index]
  before_action :form_fields, only: %i[edit update new create]

  def index
    args = pagination_params
    @articles = Article.page(args[:page]).per(args[:per])
    @total = Article.count
    respond_to do |format|
      format.html
      format.json
    end
  end

  def new
    result = ::Rewrite::Task::Result.find(params[:result_id])
    redirect_to(action: :edit, id: result.article.id) and return if result.article

    add_breadcrumb('New', :new_cms_article_path)
    @article = result.initialize_article
  rescue ActiveRecord::RecordNofFound
    redirect_to(action: :index)
  end

  def create
    @article = Article.new(article_params)
    # TODO: сделать нормальную систему публикации
    @article.publish_at = DateTime.now
    @article.save!
    redirect_to(action: :index)
  rescue ActiveRecord::RecordInvalid => e
    add_breadcrumb('New', :new_cms_article_path)
    @errors = e.record.errors.messages
    render(action: :new)
  end

  def edit
    @article = Article.find(params[:id])
    add_breadcrumb(@article.title, edit_cms_article_path(@article.id))
  rescue ActiveRecord::RecordNofFound
    redirect_to(action: :index)
  end

  def update
    @article = Article.find(params[:id])
    @article.update!(article_params)
    redirect_to(action: :index)
  rescue ActiveRecord::RecordNofFound
    redirect_to(action: :index)
  rescue ActiveRecord::RecordInvalid => e
    add_breadcrumb(@article.title, edit_cms_article_path(@article.id))
    @errors = e.record.errors.messages
    render(action: :edit)
  end

  # TODO: добавить обработчик неудачного удаления
  def destroy
    @article = Article.find(params[:id])
    @article.destroy!
    redirect_to(action: :index)
  rescue ActiveRecord::RecordNofFound
    redirect_to(action: :index)
  end

  private

    def article_params
      params.require(:article).permit(:title, :description, :body, :status, :rewrite_task_result_id)
    end

    def table_headers
      @table_headers = [{
        field: 'id',
        title: '#',
      }, {
        field: 'title',
        title: 'Title',
      }, {
        field: 'status',
        title: 'Status',
      }, {
        field: 'publish_at',
        title: 'Publish at',
      },]
    end

    # Поля для формы
    def form_fields
      @form_fields = [
        {
          type: FormFieldType::HIDDEN,
          key: :rewrite_task_result_id,
        },
        {
          type: FormFieldType::TEXT_FIELD,
          key: :title,
          label: 'Title',
        },
        {
          type: FormFieldType::TEXT_FIELD,
          key: :description,
          label: 'Description',
        },
        {
          type: FormFieldType::RICH_TEXT,
          key: :body,
          label: 'Body',
          id: 'article_body',
        },
        {
          type: FormFieldType::SELECT,
          key: :status,
          label: 'Status',
          include_blank: false,
          options: ::Article::STATUS_SELECT_OPTIONS,
        },
      ]
    end
end
