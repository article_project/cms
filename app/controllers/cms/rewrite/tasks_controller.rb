class Cms::Rewrite::TasksController < CmsController
  include BootstrapTable

  add_breadcrumb('Tasks', :cms_rewrite_tasks_path)
  before_action :table_headers, only: %i[index]
  before_action :form_fields, only: %i[edit update new create publish change_template_on_form]

  def index
    args = pagination_params
    @tasks = ::Rewrite::Task.page(args[:page]).per(args[:per])
    @total = ::Rewrite::Task.count
    respond_to do |format|
      format.html
      format.json
    end
  end

  def new
    p_article = ::Parsing::Article.find(params[:p_article_id])
    unless p_article.rewrite_task.nil?
      redirect_to(action: :edit, id: p_article.rewrite_task.id)
      return
    end
    add_breadcrumb('New task', :new_cms_rewrite_task_path)
    @task = ::Rewrite::Task.new(parsing_article_id: p_article.id)
  rescue ActiveRecord::RecordNotFound
    redirect_to(action: :index)
  end

  def create
    @task = ::Rewrite::Task.new(task_params)
    @task.save!
    redirect_to(action: :edit, id: @task.id)
  rescue ActiveRecord::RecordInvalid => e
    add_breadcrumb('New task', :new_cms_rewrite_task_path)
    @errors = e.record.errors.messages
    render(action: :new)
  end

  def edit
    @task = ::Rewrite::Task.find(params[:id])
    add_breadcrumb(@task.title, edit_cms_rewrite_task_path(@task.id))
  rescue ActiveRecord::RecordNotFound
    redirect_to(action: :index)
  end

  def update
    @task = ::Rewrite::Task.find(params[:id])
    add_breadcrumb(@task.title, edit_cms_rewrite_task_path(@task.id))
    @task.update!(task_params)
    redirect_to(action: :edit, id: @task.id)
  rescue ActiveRecord::RecordNotFound
    redirect_to(action: :index)
  rescue ActiveRecord::RecordInvalid => e
    @errors = e.record.errors.messages
    render(action: :edit, status: 422)
  end

  # TODO: сохранять ошибки в redis или переделать форму для отображения ошибок с помощью js
  def publish
    @task = ::Rewrite::Task.find(params[:task_id])
    add_breadcrumb(@task.title, edit_cms_rewrite_task_path(@task.id))
    @task.publish!
    redirect_to(action: :index)
  rescue StandardError
    redirect_to(action: :edit, id: @task.id)
    # rescue ActiveRecord::RecordNotFound
    #   redirect_to(action: :index)
    # rescue ActiveRecord::RecordInvalid => e
    #   @errors = e.record.errors.messages
    #   render(action: :edit, status: 422)
    # rescue Concern::Rewrite::Task::Market::InvalidResponse => e
    #   @errors = {error: [e.message]}
    #   render(action: :edit, status: 422)
    # rescue => e
    #   @errors = {error: [e.message]}
    #   render(action: :edit, status: 422)
  end

  def change_template_on_form
    @task = ::Rewrite::Task.find_by(id: params[:task_id])
    @task ||= ::Rewrite::Task.new(parsing_article_id: params[:parsing_article_id])
    begin
      template = ::Rewrite::Task::Template.find(params[:template_id])
      @task.template_id = template.id
      @task.fill_fields_from_template
    rescue ActiveRecord::RecordNotFound
      @task.template_id = nil
      @task.title = ''
      @task.description = ''
      @task.price_1ks = nil
      @task.deadline_days = nil
    end

    respond_to do |format|
      format.js
    end
  end

  private

    def table_headers
      @table_headers = [{
        field: 'id',
        title: '#',
      }, {
        field: 'title',
        title: 'Title',
      }, {
        field: 'status',
        title: 'Status',
      }, {
        field: 'deadline',
        title: 'Deadline',
      }, {
        field: 'source',
        title: 'Source',
      }, {
        field: 'results',
        title: 'Results',
      },]
    end

    def task_params
      params.require(:rewrite_task).permit(
        :title,
        :description,
        :price_1ks,
        :deadline_days,
        :template_id,
        :parsing_article_id,
        :status,
        :category_id
      )
    end

    # Поля для формы
    def form_fields
      @form_fields = [
        {
          type: FormFieldType::SELECT,
          key: :template_id,
          label: 'Template',
          include_blank: true,
          options: ::Rewrite::Task::Template.select_options,
        },
        {
          type: FormFieldType::TEXT_FIELD,
          key: :title,
          label: 'Title',
        },
        {
          type: FormFieldType::TEXT_AREA,
          key: :description,
          label: 'Description',
        },
        {
          type: FormFieldType::NUMBER_FIELD,
          key: :price_1ks,
          label: 'Price for 1 ks',
        },
        {
          type: FormFieldType::NUMBER_FIELD,
          key: :deadline_days,
          label: 'Deadline days',
        },
        {
          type: FormFieldType::SELECT,
          key: :status,
          label: 'Status',
          include_blank: false,
          options: ::Rewrite::Task::status_select_options,
        },
        {
          type: FormFieldType::HIDDEN,
          key: :parsing_article_id,
        },
        {
          type: FormFieldType::SELECT,
          key: :category_id,
          label: 'Category',
          include_blank: false,
          options: ::Rewrite::Category.select_options,
        },
      ]
    end
end
