class Cms::Rewrite::TaskTemplatesController < CmsController
  include BootstrapTable

  add_breadcrumb('Rewrite task templates', :cms_rewrite_task_templates_path)
  before_action :table_headers, only: %i[index]
  before_action :form_fields, only: %i[edit update new create]

  def index
    args = pagination_params
    @templates = ::Rewrite::Task::Template.page(args[:page]).per(args[:per])
    @total = ::Rewrite::Task::Template.count

    respond_to do |format|
      format.html
      format.json
    end
  end

  def new
    add_breadcrumb('New task template', :new_cms_rewrite_task_template_path)
    @template = ::Rewrite::Task::Template.new
  end

  def create
    add_breadcrumb('New task template', :new_cms_rewrite_task_template_path)

    # Создание в 2 шага что бы передать форме валидные данные а не nil (в случае ошибки валидации)
    @task = ::Rewrite::Task::Template.new(template_params)
    @task.save!

    redirect_to(action: :index)
  rescue ActiveRecord::RecordNotFound
    redirect_to(:cms_rewrite_task_templates_path)
  rescue ActiveRecord::RecordInvalid => e
    @errors = e.record.errors.messages
    render(action: :new, status: 422)
  end

  def edit
    @template = ::Rewrite::Task::Template.find(params[:id])
    add_breadcrumb(@template.title, edit_cms_rewrite_task_template_path(@template.id))
  rescue ActiveRecord::RecordNotFound
    redirect_to(:cms_rewrite_task_templates_path)
  end

  def update
    @template = ::Rewrite::Task::Template.find(params[:id])
    add_breadcrumb(@template.title, edit_cms_rewrite_task_template_path(@template.id))
    @template.update!(template_params)
    redirect_to(action: :index)
  rescue ActiveRecord::RecordNotFound
    redirect_to(:cms_rewrite_task_templates_path)
  rescue ActiveRecord::RecordInvalid => e
    @errors = e.record.errors.messages
    render(action: :edit, status: 422)
  end

  private

    def template_params
      params.require(:rewrite_task_template).permit(
        :title,
        :task_title,
        :task_description,
        :price_1ks,
        :deadline_days,
        :category_id
      )
    end

    # Хедеры для таблицы на странице index
    def table_headers
      @table_headers = [{
        field: 'id',
        title: '#',
      }, {
        field: 'title',
        title: 'Title',
      }, {
        field: 'price_1ks',
        title: 'Price for 1k symbols',
      }, {
        field: 'tasks',
        title: 'Tasks',
      },]
    end

    # Поля для формы
    def form_fields
      @form_fields = [{
        type: FormFieldType::TEXT_FIELD,
        key: :title,
        label: 'Title',
      }, {
        type: FormFieldType::TEXT_FIELD,
        key: :task_title,
        label: 'Task title',
      }, {
        type: FormFieldType::TEXT_AREA,
        key: :task_description,
        label: 'Task description',
      }, {
        type: FormFieldType::NUMBER_FIELD,
        key: :price_1ks,
        label: 'Price for 1k symbols',
      }, {
        type: FormFieldType::NUMBER_FIELD,
        key: :deadline_days,
        label: 'Deadline days',
      }, {
        type: FormFieldType::SELECT,
        key: :category_id,
        label: 'Category',
        include_blank: false,
        options: ::Rewrite::Category.select_options,
      },]
    end
end
