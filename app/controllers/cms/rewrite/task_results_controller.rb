class Cms::Rewrite::TaskResultsController < CmsController
  include BootstrapTable

  before_action :table_headers, only: %i[index]
  before_action :decline_form_fields, only: %i[show]

  def index
    task = ::Rewrite::Task.find(params[:task_id])
    basic_breadcrumbs(task)
    args = pagination_params
    @results = task.results.page(args[:page]).per(args[:per])
    @total = task.results.count
    respond_to do |format|
      format.html
      format.json
    end
  rescue ActiveRecord::RecordNotFound
    redirect_to(cms_rewrite_tasks_path)
  end

  def show
    @result = ::Rewrite::Task::Result.find(params[:id])
    basic_breadcrumbs(@result.task)
    add_breadcrumb(@result.external_id, cms_rewrite_task_result_path(@result.id))
  rescue ActiveRecord::RecordNotFound
    redirect_to(cms_rewrite_tasks_path)
  end

  def accept
    @result = ::Rewrite::Task::Result.find(params[:task_result_id])
    @result.task.accept!
    redirect_to(edit_cms_rewrite_task_path(@result.task_id))
  rescue ActiveRecord::RecordNotFound
    redirect_to(cms_rewrite_tasks_path)
  end

  def decline
    @result = ::Rewrite::Task::Result.find(params[:task_result_id])
    ::Rewrite::Task::Result.transaction do
      @result.update!(result_params)
      @result.task.decline!
    end
    redirect_to(edit_cms_rewrite_task_path(@result.task_id))
  rescue ActiveRecord::RecordNotFound
    redirect_to(cms_rewrite_tasks_path)
  end

  private

    def result_params
      params.require(:rewrite_task_result).permit(:my_comment)
    end

    def basic_breadcrumbs(task)
      add_breadcrumb('Tasks', :cms_rewrite_tasks_path)
      add_breadcrumb(task.title, edit_cms_rewrite_task_path(task.id))
      add_breadcrumb('Results', cms_rewrite_task_results_path(task.id))
    end

    def table_headers
      @table_headers = [{
        field: 'id',
        title: '#',
      }, {
        field: 'external_id',
        title: 'External id',
      }, {
        field: 'made_at',
        title: 'Made at',
      }, {
        field: 'status',
        title: 'Status',
      },]
    end

    def decline_form_fields
      @form_fields = [
        {
          type: FormFieldType::TEXT_FIELD,
          key: :my_comment,
          label: 'Comment',
        },
      ]
    end
end
