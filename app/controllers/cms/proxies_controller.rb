class Cms::ProxiesController < CmsController
  include BootstrapTable

  add_breadcrumb('Proxies', :cms_proxies_path)
  before_action :table_headers, only: %i[index]
  before_action :form_fields, only: %i[edit update new create]

  def index
    args = pagination_params
    @proxies = Proxy.page(args[:page]).per(args[:per])
    @total = Proxy.count
    respond_to do |format|
      format.html
      format.json
    end
  end

  def new
    add_breadcrumb('New', :new_cms_proxy_path)
    @proxy = Proxy.new
  end

  def create
    @proxy = Proxy.new(proxy_params)
    @proxy.save!
    redirect_to(action: :index)
  rescue ActiveRecord::RecordInvalid => e
    add_breadcrumb('New', :new_cms_proxy_path)
    @errors = e.record.errors.messages
    render(action: :new)
  end

  def edit
    @proxy = Proxy.find(params[:id])
    add_breadcrumb(@proxy.full_address, edit_cms_proxy_path(@proxy))
  rescue ActiveRecord::RecordNofFound
    redirect_to(action: :index)
  end

  def update
    @proxy = Proxy.find(params[:id])
    @proxy.update!(proxy_params)
    redirect_to(action: :index)
  rescue ActiveRecord::RecordNofFound
    redirect_to(action: :index)
  rescue ActiveRecord::RecordInvalid => e
    add_breadcrumb(@proxy.full_address, edit_cms_article_path(@article.id))
    @errors = e.record.errors.messages
    render(action: :edit)
  end

  # TODO: добавить обработчик неудачного удаления
  def destroy
    @proxy = Proxy.find(params[:id])
    @proxy.destroy!
    redirect_to(action: :index)
  rescue ActiveRecord::RecordNofFound
    redirect_to(action: :index)
  end

  def parse
    Proxy.parse!
    redirect_to(action: :index)
  end

  private

    def proxy_params
      params.require(:proxy).permit(:ip, :port)
    end

    def table_headers
      @table_headers = [{
        field: 'id',
        title: '#',
      }, {
        field: 'full_address',
        title: 'Full address',
      },]
    end

    # Поля для формы
    def form_fields
      @form_fields = [
        {
          type: FormFieldType::TEXT_FIELD,
          key: :ip,
          label: 'IP',
        },
        {
          type: FormFieldType::NUMBER_FIELD,
          key: :port,
          label: 'Port',
        },
      ]
    end
end
