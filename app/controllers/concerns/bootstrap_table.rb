module BootstrapTable
  extend ActiveSupport::Concern

  def pagination_params
    args = params.permit(:offset, :limit)
    result = {
      page: 1,
      per: 5,
    }
    if args[:offset] && args[:limit]
      result[:page] = (args[:offset].to_i / args[:limit].to_i) + 1
      result[:per] = args[:limit].to_i
    end
    result
  end
end
