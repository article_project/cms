class Rewrite::Task::Template < ApplicationRecord
  def self.table_name_prefix
    'task_'
  end

  has_many :tasks, class_name: 'Rewrite::Task', foreign_key: 'template_id'
  belongs_to :category, class_name: 'Rewrite::Category'

  validates :title, presence: true
  validates :task_title, presence: true
  validates :price_1ks, presence: true
  validates :deadline_days, presence: true

  def task_params
    {
      template_id: self.id,
      title: self.task_title,
      description: self.task_description,
      price_1ks: self.price_1ks,
      deadline_days: self.deadline_days,
      category_id: self.category_id,
    }
  end

  def self.select_options
    Rewrite::Task::Template.all.collect { |template| [template.title, template.id] }
  end
end
