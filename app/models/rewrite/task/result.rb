require 'open-uri'

class Rewrite::Task::Result < ApplicationRecord
  include Concern::Rewrite::Task::Result::Status

  belongs_to :task, class_name: 'Rewrite::Task'

  has_one :article, foreign_key: 'rewrite_task_result_id'

  validates :external_id, presence: true
  validates :made_at, presence: true
  validates :result_url, presence: true
  validates :system_checked, inclusion: { in: [true, false] }

  before_save :load_result, if: Proc.new { |result| !result.result_url.blank? && self.result_url_changed? }

  def initialize_article
    ::Article.new(rewrite_task_result_id: self.id,
                  body: self.result,
                  title: self.task.parsing_article.title,
                  description: self.description)
  end

  def load_result
    doc = Docx::Document.open(open(self.result_url))
    self.update_column(:result, doc.to_s.gsub(/\n/, '<br/>'))
  rescue => e
    Rails.logger.error("Rewrite::Task::Result::load_result exception: #{e}")
  end

  def description
    desc = self.result[0..100].gsub(/<br\/>/, ' ').split(' ')
    desc.pop
    desc.join(' ')
  end
end
