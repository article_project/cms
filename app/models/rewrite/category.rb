class Rewrite::Category < ApplicationRecord
  default_scope { order('title asc') }

  has_many :task_templates, class_name: 'Rewrite::Task::Template', foreign_key: 'category_id'
  has_many :tasks, class_name: 'Rewrite::Task', foreign_key: 'category_id'

  def self.select_options
    Rewrite::Category.all.map { |category| [category.title, category.id] }
  end
end
