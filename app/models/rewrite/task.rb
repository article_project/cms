class Rewrite::Task < ApplicationRecord
  include Concern::Rewrite::Task::Status
  include Concern::Rewrite::Task::Market

  belongs_to :parsing_article, class_name: 'Parsing::Article'
  belongs_to :template, class_name: 'Rewrite::Task::Template', required: false
  belongs_to :category, class_name: 'Rewrite::Category'
  has_many :results, class_name: 'Rewrite::Task::Result', foreign_key: 'task_id'

  validates :title, presence: true
  validates :price_1ks, presence: true
  validates :status, presence: true
  validates :deadline_days, presence: true
  validates :parsing_article_id, uniqueness: true
  validate :check_deadline

  before_validation :fill_fields_from_template
  after_create_commit :change_article_status

  def current_result
    self.results.on_checking.first
  end

  def fill_fields_from_template
    template = self.template || ::Rewrite::Task::Template.find(self.template_id)
    self.assign_attributes(template.task_params, override: false)
  rescue ActiveRecord::RecordNotFound
    self.description = '' if self.description.nil?
    return
  end

  private

    def change_article_status
      self.parsing_article.update(status: Parsing::Article::SENT_TO_REWRITE)
    end

    def check_deadline
      if !self.new? && !self.publishing_error? && self.deadline.nil?
        errors.add(
          :deadline,
          'Deadline can not be nil when status not new'
        )
      end
    end
end
