class Article < ApplicationRecord
  include Concern::Article::Status
  include Rails.application.routes.url_helpers

  default_scope { where.not(status: DELETED).order(publish_at: :desc) }

  belongs_to :rewrite_task_result, class_name: 'Rewrite::Task::Result', required: false

  has_one_attached :image

  validates :title, presence: true, uniqueness: true
  validates :body, presence: true, uniqueness: true
  validates :publish_at, presence: true

  validate :title_not_equal_to_parse_title

  before_create :set_publish_at
  before_create :set_image

  before_validation :set_link

  before_save :clear_body

  def json_for_api
    self.as_json(
      only: %i[title description link publish_at body]
    ).merge('image' => url_for(self.image))
  end

  # TODO: переместить это в parsing article и убрать из сидов атрибуты
  def clear_body
    self.body = self.body.gsub(%r{( [\w\-]*(?<!src)=\"[а-яА-Я\w\d\s\-?,\.#';()=/:]*\")}, '')
  end

  private

    def set_image
      self.image.attach(
        io: File.open(Rails.root + 'public/default_article_image.png'),
        filename: 'default_article_image.png',
        content_type: 'image/png'
      )
    end

    def set_publish_at
      self.publish_at = DateTime.now if self.publish_at.nil?
    end

    def set_link
      # self.link = self.title.parameterize
    end

    def title_not_equal_to_parse_title
      return if self.rewrite_task_result.nil?

      if self.title == self.rewrite_task_result.task.parsing_article.title
        errors.add(:title, 'title must not be equal to parse article title')
      end
    end
end
