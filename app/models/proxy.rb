class Proxy < ApplicationRecord
  include Concern::Proxy::Web

  def full_address
    "http://#{self.ip}:#{self.port}"
  end
end
