module Concern::Article::Status
  extend ActiveSupport::Concern

  CLOSED = 0
  OPEN = 1
  DELETED = 2

  STATUS_NAMES = %w[
    Closed
    Open
    Deleted
  ].freeze

  STATUS_SELECT_OPTIONS = [
    [STATUS_NAMES[CLOSED], CLOSED],
    [STATUS_NAMES[OPEN], OPEN],
    [STATUS_NAMES[DELETED], DELETED],
  ].freeze

  included do
    scope :closed, -> { where(status: CLOSED) }
    scope :open, -> { where(status: OPEN) }
    scope :deleted, -> { where(status: DELETED) }

    validates :status, presence: true
  end

  def closed?
    self.status == CLOSED
  end

  def open?
    self.status == OPEN
  end

  def deleted?
    self.status == DELETED
  end

  def status_to_s
    STATUS_NAMES[self.status]
  end
end
