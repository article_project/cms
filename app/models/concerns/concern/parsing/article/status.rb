module Concern::Parsing::Article::Status
  extend ActiveSupport::Concern

  included do
    NEW = 0
    NOT_ARTICLE = 1
    FOR_REWRITE = 2
    SENT_TO_REWRITE = 3
    REWRITED = 4

    scope :active, -> { where.not(status: NOT_ARTICLE) }
    scope :news, -> { where(status: NEW) }
    scope :not_article, -> { where(status: NOT_ARTICLE) }
    scope :for_rewrite, -> { where(status: FOR_REWRITE) }
    scope :sent_to_rewrite, -> { where(status: SENT_TO_REWRITE) }
    scope :rewrited, -> { where(status: REWRITED) }
  end

  def new?
    self.status == NEW
  end

  def not_article?
    self.status == NOT_ARTICLE
  end

  def for_rewrite?
    self.status == FOR_REWRITE
  end

  def sent_to_rewrite?
    self.status == SENT_TO_REWRITE
  end

  def rewrited?
    self.status == REWRITED
  end

  def status_to_s
    case self.status
    when NEW
      'New'
    when NOT_ARTICLE
      'Not an article'
    when FOR_REWRITE
      'For rewrite'
    when SENT_TO_REWRITE
      'Sent to rewrite'
    when REWRITED
      'Rewrited'
    end
  end
end
