module Concern::Parsing::Task::Status
  extend ActiveSupport::Concern

  included do
    INACTIVE = 0
    ACTIVE = 1
    ARCHIVED = 2

    scope :active, -> { where(status: ACTIVE) }
    scope :inactive, -> { where(status: INACTIVE) }
    scope :archived, -> { where(status: ARCHIVED) }
  end

  def active?
    self.status == ACTIVE
  end

  def inactive?
    self.status == INACTIVE
  end

  def archived?
    self.status == ARCHIVED
  end

  def status_to_s
    case self.status
    when INACTIVE
      'Inactive'
    when ACTIVE
      'Active'
    when ARCHIVED
      'Archived'
    end
  end
end
