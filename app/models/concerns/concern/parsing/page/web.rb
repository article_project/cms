require 'open-uri'

# TODO: переместить в отдельный сервис
# TODO: вместо open-uri использовать capybara
# rubocop:disable Security/Open
module Concern::Parsing::Page::Web
  extend ActiveSupport::Concern

  included do
    scope :not_collected_hrefs, -> { where(hrefs_has_been_parsed: false) }
    scope :not_collected_articles, -> { where(articles_has_been_parsed: false) }
  end

  def compare_host(href_param)
    URI(href_param).host == URI(self.address).host
  rescue StandardError
    false
  end

  def collect_hrefs
    self.class.collect_hrefs(self)
  end

  def collect_articles
    self.class.collect_articles(self)
  end

  def body
    open(self.address).read
  end

  class_methods do
    def collect_hrefs(page)
      return if page.nil? || !page.task.active? || page.hrefs_has_been_parsed

      # TODO: пофиксить
      nokogiri_body = Parsing::Page.get_response_body(page)
      return if nokogiri_body.nil?

      # создаем адреса из спаршенной страницы (итерация по всем элементам, ищем атрибут href)
      nokogiri_body.css('*').each do |node|
        next if node.attribute('href').nil? || !page.compare_host(node.attribute('href').value)

        begin
          Parsing::Page.create(task_id: page.task_id, address: node.attribute('href').value)
        rescue ActiveRecord::RecordInvalid
          Rails.logger.warn("Concern::Parsing::Page::Web: page with id #{page.id} has invalid nested page")
        end
      end

      page.update(hrefs_has_been_parsed: true)
    end

    def collect_articles(page)
      return if page.nil? || !page.task.active? || page.articles_has_been_parsed

      # TODO: пофиксить
      nokogiri_body = Parsing::Page.get_response_body(page)
      return if nokogiri_body.nil?

      title = nokogiri_body.css(page.task.article_title_path)
                           .first&.inner_html&.slice(0, 127) || 'No Title'
      body = nokogiri_body.css(page.task.article_body_path)
                          .first&.inner_html
      if body.blank?
        Rails.logger.info("Concern::Parsing::Page::Web: page with id #{page.id} has no article")
      else
        begin
          Parsing::Article.create(
            title: Sanitize.clean(title).gsub('&nbsp;', ' ').strip,
            body: Sanitize.clean(body).gsub('&nbsp;', ' ').strip,
            page_id: page.id
          )
        rescue ActiveRecord::RecordInvalid
          # Просто игнорим
          Rails.logger.warn("Concern::Parsing::Page::Web: page with id #{page.id} has invalid article")
        end
      end

      page.update(articles_has_been_parsed: true)
    end

    def get_response_body(page)
      response_body = open(page.address).read
      body = Nokogiri::HTML(response_body)

      # TODO: пофиксить
      if Parsing::Page.response_body_correct?(page, body)
        body
      else
        Parsing::Page.try_to_get_response_body_with_proxy(page)
      end
    # TODO: логировать это в CMS
    rescue OpenURI::HTTPError
      page.update(hrefs_has_been_parsed: true)
      page.update(articles_has_been_parsed: true)
      nil
    end

    def try_to_get_response_body_with_proxy(page)
      destroy_proxies = []
      Proxy.all.each do |proxy|
        response_body = open(page.address, proxy: URI.parse(proxy.full_address)).read
        body = Nokogiri::HTML(response_body)
        return body if Parsing::Page.response_body_correct?(page, body)
      # TODO: логировать это
      rescue Errno::ECONNREFUSED, Net::OpenTimeout, OpenSSL::SSL::SSLError
        destroy_proxies << proxy
      # не знаю что это за ошибка, но прокси должен работать
      rescue EOFError
        Rails.logger.error('Concern::Parsing::Page::Web EOFError')
      end
      destroy_proxies.each(&:destroy)
      nil
    end

    def response_body_correct?(page, body)
      # если правила не указаны, то просто возвращаем true
      if page.task.block_request_path.blank? && page.task.block_request_value.blank?
        true
      # если в правилах указан только путь, то проверяем наличие любого значение по этому пути
      elsif !page.task.block_request_path.blank? && page.task.block_request_value.blank?
        body.css(page.task.block_request_path).first&.inner_html&.blank?
      # иначе проверяем на соответствие значения по этому пути
      else
        !body.css(page.task.block_request_path).first&.inner_html&.include?(page.task.block_request_value)
      end
    end
  end
end
# rubocop:enable Security/Open
