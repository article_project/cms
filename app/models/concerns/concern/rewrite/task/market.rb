require 'digest/md5'

# etxt api
module Concern::Rewrite::Task::Market
  extend ActiveSupport::Concern

  API_URL = 'https://www.etxt.ru/api/json/'.freeze

  class InvalidResponse < StandardError; end
  class ResponseReturnsInvalidIds < StandardError; end

  def external_task_link
    "https://www.etxt.ru/admin.php?mod=tasks&lib=task&act=view&id_task=#{self.external_id}"
  end

  def publish!
    deadline = self.deadline || DateTime.now.to_date + self.deadline_days.day

    options = Rewrite::Task.basic_options('tasks.saveTask')
    options[:body] = {
      id: self.external_id,
      public: Rails.env.production? ? 1 : 0, # не публикуется
      title: self.title,
      description: self.description,
      text: self.parsing_article.body,
      price: self.price_1ks,
      price_type: 1, # цена за 1 килосимвол
      uniq: 95, # уникальность заказа
      whitespaces: 0, # цена без пробелов
      checksize: 1, # тексты менее 90% от поля text не принимаются
      id_type: 2,
      deadline: deadline.strftime('%d.%m.%Y'),
      timeline: '23:59',
      auto_work: 1, # принимаем заявки автоматически
      id_category: self.category_id, # TODO: нужно установить верное значение
      multitask: 0, # обычный заказ
      target_task: 1, # заказ доступен для всех
    }
    options[:body].compact! # если external_id nil, то его следует удалить что бы биржа сгенерировала новый

    response = HTTParty.post(API_URL, options)
    response_json = JSON.parse(response.parsed_response)

    if response_json['id_task'].blank?
      raise Concern::Rewrite::Task::Market::InvalidResponse.new({ response: response }.to_json)
    else
      self.external_id = response_json['id_task'].to_i
      self.deadline = deadline
      if self.status == Concern::Rewrite::Task::Status::NEW ||
         self.status == Concern::Rewrite::Task::Status::PUBLISHING_ERROR
        self.status = Concern::Rewrite::Task::Status::SEARCHING_FOR_EXECUTOR
      end
    end
  rescue Concern::Rewrite::Task::Market::InvalidResponse => e
    Rails.logger.error("Concern::Rewrite::Task::Market::publish!: invalid response, response: #{response}")
    self.status = Concern::Rewrite::Task::Status::PUBLISHING_ERROR
    throw(e)
  rescue StandardError => e
    Rails.logger.error("Concern::Rewrite::Task::Market::publish!: unknown error or invalid sign, response: #{response}")
    self.status = Concern::Rewrite::Task::Status::PUBLISHING_ERROR
    throw(e)
  ensure
    self.save!
  end

  def accept!
    return if self.current_result.nil? || self.current_result.accepted?

    if Rails.env != 'production'
      self.current_result.update!(status: Rewrite::Task::Result::ACCEPTED)
      self.update(status: Rewrite::Task::DONE)
      return
    end

    options = Rewrite::Task.basic_options('tasks.paidTask')
    options[:body] = {
      id: [self.external_id],
      text: self.current_result.my_comment.blank? ? nil : self.my_comment,
    }.compact

    response = HTTParty.post(API_URL, options)
    response_json = JSON.parse(response.parsed_response)

    if response_json['ids'].include?(self.external_id) && response_json['status'].to_i == 1
      self.current_result.update!(status: Rewrite::Task::Result::ACCEPTED)
      self.update(status: Rewrite::Task::DONE)
    else
      raise Concern::Rewrite::Task::Market::ResponseReturnsInvalidIds.new
    end
  rescue Concern::Rewrite::Task::Market::ResponseReturnsInvalidIds
    Rails.logger.error("Concern::Rewrite::Task::Market::accept!: response: #{response}")
  end

  def decline!
    return if self.current_result.nil? || self.current_result.accepted?

    if Rails.env != 'production'
      self.current_result.update!(status: Rewrite::Task::Result::DECLINED)
      return
    end

    options = Rewrite::Task.basic_options('tasks.cancelTask')
    options[:body] = {
      id: [self.external_id],
      text: self.current_result.my_comment.blank? ? nil : self.my_comment,
    }.compact

    response = HTTParty.post(API_URL, options)
    response_json = JSON.parse(response.parsed_response)

    if response_json['ids'].include?(self.external_id) && response_json['status'].to_s == 1
      self.current_result.update!(status: Rewrite::Task::Result::DECLINED)
    else
      raise Concern::Rewrite::Task::Market::ResponseReturnsInvalidIds.new
    end
  rescue Concern::Rewrite::Task::Market::ResponseReturnsInvalidIds
    Rails.logger.error("Concern::Rewrite::Task::Market::accept!: response: #{response}")
  end

  module ClassMethods
    def status
      target_ids = Rewrite::Task.active.pluck(:external_id)
      return if target_ids.blank?

      options = basic_options('tasks.listTasks')
      options[:body] = {
        id: target_ids,
      }

      response = HTTParty.post(API_URL, options)
      response_json = JSON.parse(response.parsed_response)

      response_json.each do |result|
        begin
          task = Rewrite::Task.find_by!(external_id: result['id'])
          task.update!(status: Rewrite::Task.from_market_status(result['status']))
        rescue ActiveRecord::RecordNotFound
          Rails.logger.error("Concern::Rewrite::Task::Market::status: task with external_id #{result['id']} not found")
        rescue ActiveRecord::RecordInvalid => e
          Rails.logger.error("Concern::Rewrite::Task::Market::status: update failed, exception: #{e}")
        end
      end
    end

    def results
      target_ids = Rewrite::Task.check_result.pluck(:external_id)
      return if target_ids.blank?

      options = basic_options('tasks.getResults')
      options[:body] = {
        id: target_ids,
      }

      response = HTTParty.post(API_URL, options)
      response_json = JSON.parse(response.parsed_response)

      response_json.each do |task_key, task_result|
        task_result.each do |key, result|
          task_result = Rewrite::Task::Result.where(external_id: result['id']).first_or_initialize
          task_result.assign_attributes(
            task_id: Rewrite::Task.find_by!(external_id: result['id_task']).id,
            performer_comment: result['comment'],
            made_at: Time.at(result['date'].to_i).to_datetime,
            status: result['status'],
            system_checked: result['check'],
            per_mistakes: result['per_mistakes'],
            per_antiplagiat: result['per_antiplagiat'],
            per_diff: result['per_diff'],
            result_url: result['files']['file']['path']
          )
          task_result.save!
        rescue ActiveRecord::RecordNotFound
          Rails.logger.error('Concern::Rewrite::Task::Market::results: record not found')
        rescue ActiveRecord::RecordInvalid => e
          Rails.logger.error(
            <<-EOF
              Concern::Rewrite::Task::Market::results: record invalid,
              external_id: #{e.record.external_id},
              errors: #{e.record.errors.messages}
            EOF
          )
        end
      end
    end

    def basic_options(method_name)
      sign_args = {
        method: method_name,
        token: Rails.application.credentials.market[:token],
      }

      {
        query: {
          method: method_name,
          token: Rails.application.credentials.market[:token],
          sign: sign_params(sign_args),
        },
      }
    end

    def sign_params(params)
      formed_params = ''
      params = params.sort
      params.each do |param|
        formed_params += "#{param[0]}=#{param[1]}"
      end
      sign_key = Digest::MD5.hexdigest("#{Rails.application.credentials.market[:pass_key]}api-pass")
      Digest::MD5.hexdigest("#{formed_params}#{sign_key}")
    end
  end
end
