module Concern::Rewrite::Task::Result::Status
  extend ActiveSupport::Concern

  ON_CHECKING = 0
  DECLINED = 1
  ACCEPTED = 2

  STATUS_NAMES = [
    'On checking',
    'Declined',
    'Accepted',
  ].freeze

  included do
    scope :on_checking, -> { where(status: ON_CHECKING) }
    scope :declined, -> { where(status: DECLINED) }
    scope :accepted, -> { where(status: ACCEPTED) }

    validates :status, presence: true, inclusion: { in: 0..2 }
  end

  def on_checking?
    self.status == ON_CHECKING
  end

  def declined?
    self.status == DECLINED
  end

  def accepted?
    self.status == ACCEPTED
  end

  def status_to_s
    STATUS_NAMES[self.status]
  end
end
