module Concern::Rewrite::Task::Status
  extend ActiveSupport::Concern

  included do
    NEW = 0
    SEARCHING_FOR_EXECUTOR = 1
    WORK_IN_PROGRESS = 2
    DONE = 3
    EXPIRED = 4
    PUBLISHING_ERROR = 5
    CHECK_RESULT = 6

    scope :active, -> { where(status: [SEARCHING_FOR_EXECUTOR, WORK_IN_PROGRESS]) }
    scope :news, -> { where(status: NEW) }
    scope :searching_for_executor, -> { where(status: SEARCHING_FOR_EXECUTOR) }
    scope :work_in_progress, -> { where(status: WORK_IN_PROGRESS) }
    scope :done, -> { where(status: DONE) }
    scope :expired, -> { where(status: EXPIRED) }
    scope :publishing_error, -> { where(status: PUBLISHING_ERROR) }
    scope :check_result, -> { where(status: CHECK_RESULT) }

    def self.status_select_options
      [
        ['New', NEW],
        ['Searching for executor', SEARCHING_FOR_EXECUTOR],
        ['Work in progress', WORK_IN_PROGRESS],
        ['Done', DONE],
        ['Expired', EXPIRED],
        ['Publishing error', PUBLISHING_ERROR],
        ['Check result', CHECK_RESULT],
      ]
    end

    def self.from_market_status(market_status)
      case market_status.to_i
      when 1 # Wait for employee
        SEARCHING_FOR_EXECUTOR
      when 2 # Work in progress
        WORK_IN_PROGRESS
      when 3 # Check result
        CHECK_RESULT
      when 4 # Completed
        DONE
      when 5 # Expired
        EXPIRED
      end
    end
  end

  def new?
    self.status == NEW
  end

  def searching_for_executor?
    self.status == SEARCHING_FOR_EXECUTOR
  end

  def work_in_progress?
    self.status == WORK_IN_PROGRESS
  end

  def done?
    self.status == DONE
  end

  def expired?
    self.status == EXPIRED
  end

  def publishing_error?
    self.status == PUBLISHING_ERROR
  end

  def check_result?
    self.status == CHECK_RESULT
  end

  def status_to_s
    case self.status
    when NEW
      'New'
    when SEARCHING_FOR_EXECUTOR
      'Searching for executor'
    when WORK_IN_PROGRESS
      'Work in progress'
    when DONE
      'Done'
    when EXPIRED
      'Expired'
    when PUBLISHING_ERROR
      'Publishing error'
    when CHECK_RESULT
      'Check result'
    end
  end
end
