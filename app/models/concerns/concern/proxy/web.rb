require 'open-uri'

module Concern::Proxy::Web
  extend ActiveSupport::Concern

  PROXY_URL = 'https://free-proxy-list.net/'

  class_methods do
    def parse!
      response_body = open(PROXY_URL).read
      body = Nokogiri::HTML(response_body)
      rows = body.css(".table-responsive table tr")

      raise Exception("Concern::Proxy::Web::parse rows is blank, body: #{body}") if rows.blank?

      proxies = rows.map{ |row|
        if row.css('td').blank?
          nil
        else
          {
            ip: row.css('td')[0].inner_html,
            port: row.css('td')[1].inner_html,
          }
        end
      }.compact

      Proxy.create(proxies)
    end
  end
end