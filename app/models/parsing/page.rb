class Parsing::Page < ApplicationRecord
  include Concern::Parsing::Page::Web

  belongs_to :task, class_name: 'Parsing::Task'
  has_many :articles, class_name: 'Parsing::Article', foreign_key: 'page_id'

  validates :address, uniqueness: true

  validate :check_address

  before_validation :format_address

  def check_address
    uri = URI(self.address)
    errors.add(:address, 'Неверный формат адреса') if uri.host.nil? || uri.host != URI(self.task.host).host
  end

  def format_address
    self.update(address: "#{self.task.host}/#{self.address.gsub(%r{^/}, '')}") if URI(self.address).host.nil?
  end
end
