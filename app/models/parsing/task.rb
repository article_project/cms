class Parsing::Task < ApplicationRecord
  include Concern::Parsing::Task::Status

  has_many :pages, class_name: 'Parsing::Page', foreign_key: 'task_id'

  validates :host, uniqueness: true

  validate :check_host

  after_create :create_page

  def check_host
    uri = URI(self.host)
    errors.add(:host, 'Неверный формат хоста') if uri.host.nil?
  end

  def create_page
    Parsing::Page.create(address: self.host, task_id: self.id)
  end
end
