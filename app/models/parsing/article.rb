class Parsing::Article < ApplicationRecord
  include Concern::Parsing::Article::Status

  belongs_to :page, class_name: 'Parsing::Page'

  has_one :rewrite_task, class_name: 'Rewrite::Task', foreign_key: 'parsing_article_id'

  validates :body, uniqueness: true
  validates :title, uniqueness: { scope: :page_id }
end
