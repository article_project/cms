class RewriteTaskResult < ApplicationJob
  def perform
    Rewrite::Task.result
  end
end
