class HrefParser < ApplicationJob
  def perform
    Parsing::Task.active.each do |task|
      task.pages.not_collected_hrefs.first&.collect_hrefs
    end
  end
end
