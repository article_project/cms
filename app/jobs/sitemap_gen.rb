require 'rake'

class SitemapGen < ApplicationJob
  def perform
    CMS::Application.load_tasks
    Rake::Task['sitemap:refresh'].invoke
  end
end
