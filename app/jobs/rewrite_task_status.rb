class RewriteTaskStatus < ApplicationJob
  def perform
    Rewrite::Task.status
  end
end
