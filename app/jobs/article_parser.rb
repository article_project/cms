class ArticleParser < ApplicationJob
  def perform
    Parsing::Task.active.each do |task|
      task.pages.not_collected_articles.first&.collect_articles
    end
  end
end
