child @articles => :rows do
  attributes(:id)

  node(:title) { |article| link_to(article.title, edit_cms_article_path(article.id)) }
  node(:status, &:status_to_s)
  node(:publish_at) { |article| article.publish_at.nil? ? '' : article.publish_at.strftime('%Y.%m.%d %H:%M') }
end

node(:total) do
  @total
end
