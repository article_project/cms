child @proxies => :rows do
  attributes(:id)

  node(:full_address) { |proxy| link_to(proxy.full_address, edit_cms_proxy_path(proxy)) }
end

node(:total) do
  @total
end
