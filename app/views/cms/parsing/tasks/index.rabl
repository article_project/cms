child @tasks => :rows do
  attributes(:id)

  node(:host) { |task| link_to(task.host, task.host) }
  node(:status, &:status_to_s)
  node(:edit) { |task| link_to('Edit', edit_cms_parsing_task_path(task)) }
  node(:pages) do |task|
    link_to(task.pages.count, cms_parsing_task_pages_path(task_id: task.id))
  end
end

node(:total) do
  @total
end
