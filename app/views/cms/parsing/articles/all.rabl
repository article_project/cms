child @articles => :rows do
  attributes(:id)

  node(:address) { |article| link_to(article.page.address, article.page.address) }
  node(:status, &:status_to_s)
  node(:edit) do |article|
    link_to(
      'Edit',
      edit_cms_parsing_task_page_article_path(
        task_id: article.page.task_id,
        page_id: article.page_id,
        id: article.id
      )
    )
  end
end

node(:total) do
  @total
end
