child @pages => :rows do
  attributes(:id, :address, :hrefs_has_been_parsed, :articles_has_been_parsed)

  node(:articles) do |page|
    link_to(
      page.articles.length,
      cms_parsing_task_page_articles_path(
        page_id: page.id,
        task_id: page.task_id
      )
    )
  end
  node(:body) do |page|
    link_to(
      'Содержание',
      cms_parsing_task_page_path(
        id: page.id,
        task_id: page.task_id
      )
    )
  end
end

node(:total) do
  @total
end
