child @results => :rows do
  attributes(:external_id)

  node(:id) { |result| link_to(result.id, cms_rewrite_task_result_path(result.id)) }
  node(:made_at) { |result| result.made_at.strftime('%Y.%m.%d %H:%M') }
  node(:status, &:status_to_s)
end

node(:total) do
  @total
end
