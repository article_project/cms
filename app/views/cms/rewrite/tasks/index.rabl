child @tasks => :rows do
  attributes(:id)

  node(:title) { |task| link_to(task.title, edit_cms_rewrite_task_path(task.id)) }
  node(:status, &:status_to_s)
  node(:deadline) do |task|
    task.deadline.nil? ? "#{task.deadline_days} days" : task.deadline.strftime('%d.%m.%Y')
  end
  node(:source) do |task|
    link_to(
      task.parsing_article_id,
      edit_cms_parsing_task_page_article_path(
        page_id: task.parsing_article.page_id,
        task_id: task.parsing_article.page.task_id,
        id: task.parsing_article_id
      )
    )
  end
  node(:results) { |task| link_to(task.results.count, cms_rewrite_task_results_path(task.id)) }
end

node(:total) do
  @total
end
