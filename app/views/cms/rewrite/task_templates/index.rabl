child @templates => :rows do
  attributes(:id, :price_1ks)

  node(:title) { |template| link_to(template.title, edit_cms_rewrite_task_template_path(template)) }
  node(:tasks) { |template| template.tasks.count }
end

node(:total) do
  @total
end
