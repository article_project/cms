$(document).on("change", "#rewrite_task_template_id", function (event) {
  $.ajax({
    url: "/cms/rewrite/change_template_on_task_form",
    data: {
      authenticity_token: $('[name="csrf-token"]')[0].content,
      task_id: $("#rewrite_task_parsing_article_id").val(),
      template_id: event.currentTarget.value,
      parsing_article_id: $("#rewrite_task_parsing_article_id").val()
    },
    method: "POST"
  })
})
