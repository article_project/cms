$(document).ready(function () {
  Trix.config.attachments.preview.caption = { name: false, size: false };
});
document.addEventListener('trix-attachment-add', function (event) {
    var file = event.attachment.file;
    if (file) {
      var upload = new window.ActiveStorage.DirectUpload(file, `/rails/active_storage/direct_uploads`, window);
      upload.create((error, attributes) => {
        if (error) {
          return false;
        } else {        
          return event.attachment.setAttributes({
            url: `${window.location.origin}/rails/active_storage/blobs/${attributes.signed_id}/${attributes.filename}`,
            href: `${window.location.origin}/rails/active_storage/blobs/${attributes.signed_id}/${attributes.filename}`,
          });
        }
      });
    }
  });
