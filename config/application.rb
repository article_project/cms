require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module CMS
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    config.autoload_paths << "#{Rails.root}/app/exceptions"

    config.action_dispatch.default_headers = {
      'Access-Control-Allow-Origin' => '*',
      'Access-Control-Request-Method' => %w{GET POST}.join(",")
    }

    # Add javascript modules to assets
    config.assets.paths << Rails.root.join('node_modules')

    # Async tasks
    config.active_job.queue_adapter = :sidekiq

    I18n.default_locale = :ru

    Rails.application.routes.default_url_options[:host] = 'http://localhost:3000'
  end
end
