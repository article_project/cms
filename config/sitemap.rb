require 'rubygems'
require 'sitemap_generator'

SitemapGenerator::Sitemap.default_host = 'http://travel4ever.ru'
SitemapGenerator::Sitemap.public_path = 'sitemap/'
SitemapGenerator::Sitemap.create do
  Article.open.each do |article|
    add("/articles/#{article.link}", changefreq: 'weekly', priority: 1)
  end
end
SitemapGenerator::Sitemap.ping_search_engines
