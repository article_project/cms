class ActiveRecord::Base

  def assign_attributes(new_attributes, options={})
    return super(new_attributes) if options.fetch(:override, true)
    super(new_attributes.select {|k,_| self[k].nil? })
  end
  
end