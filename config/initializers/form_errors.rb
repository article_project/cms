# Отключаем форматирование полей с ошибками от RoR, что бы корректно работало форматирование Bootstrap
ActionView::Base.field_error_proc = Proc.new do |html_tag, instance|
  class_attr_index = html_tag.index ''

  if class_attr_index
    html_tag.insert class_attr_index+7, ''
  else
    html_tag.insert html_tag.index('>'), ''
  end
end