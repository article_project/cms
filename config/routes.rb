Rails.application.routes.draw do

  root :to => redirect('/cms') 

  namespace :cms do
    root to: 'articles#index'

    namespace :parsing do
      # tasks
      resources :tasks, only: %i[index new edit create update] do
        # pages
        get 'page/:id', to: 'pages#show', as: 'page'
        get 'page/:id/body', to: 'pages#body', as: 'page_body'
        resources :pages, only: %i[index] do
          # articles
          resources :articles, only: %i[index edit update]
        end
      end
      # articles
      get '/articles', to: 'articles#all', as: 'all_articles'
    end
    
    namespace :rewrite do
      # task_templates
      resources :task_templates, only: %i[index new edit create update]

      # tasks
      resources :tasks, only: %i[index new edit create update] do
        post 'publish', to: 'tasks#publish', as: 'publish'
      end
      post 'change_template_on_task_form', to: 'tasks#change_template_on_form', as: 'change_template_on_task_form'
      get '/parsing_articles/:p_article_id/tasks', to: 'tasks#new', as: 'new_rewrite_task'
      
      # task results
      get '/tasks/:task_id/results', to: 'task_results#index', as: 'task_results'
      resources :task_results, only: %i[show] do
        post 'accept', to: 'task_results#accept', as: 'accept'
        patch 'decline', to: 'task_results#decline', as: 'decline'
      end
    end

    # articles
    resources :articles

    # proxies
    resources :proxies
    post 'parse_proxies', to: 'proxies#parse', as: 'parse_proxies'
  end

  namespace :api do
  
    resources :articles, only: %i[index show]

  end
    
end